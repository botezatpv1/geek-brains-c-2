﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace geek_brains_c_2
{
    static class Program
    {
        static void Main()
        {
            LevelManager.ChangeLevel(LevelManager.Levels.SplashScreen);
            Application.Run();
        }
    }
}
