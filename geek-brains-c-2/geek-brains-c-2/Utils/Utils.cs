﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace geek_brains_c_2
{
    class Utils
    {
        public static Label CreateLabel(string text, Point location, Size size, int fontSize)
        {
            Label label = new Label();
            label.Text = text;
            label.Location = location;
            label.Size = size;
            label.Font = new Font("Serif", fontSize);
            return label;
        }

        public static Button CreateButton(Color backColor, string text, string name, Point location, Size size, EventHandler e)
        {
            Button button = new Button();
            button.BackColor = backColor;
            button.Text = text;
            button.Name = name;
            button.Location = location;
            button.Size = size;
            button.Click += new EventHandler(e);
            return button;
        }
    }
}
