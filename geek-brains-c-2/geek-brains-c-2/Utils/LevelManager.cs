﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace geek_brains_c_2
{
    class LevelManager
    {
        public enum Levels { SplashScreen, Game }
        private static Levels Level;

        public static Form Form;

        static Form CreateForm()
        {
            Form form = new Form();
            form.Width = 800;
            form.Height = 600;
            return form;
        }

        static void InitGame()
        {
            if (Form != null)
            {
                Render.Timer.Stop();
                Form.Dispose();
            }

            Form = CreateForm();
            if (Level == Levels.SplashScreen)
            {
                SplashScreen.Init(Form);
            }
            else if (Level == Levels.Game)
            {
                Game.Init(Form);
            }
            Form.Show();
        }

        public static void ChangeLevel(Levels level)
        {
            Level = level;
            InitGame();
        }
    }
}
