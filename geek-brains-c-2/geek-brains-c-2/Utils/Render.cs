﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace geek_brains_c_2
{
    class Render
    {
        public BufferedGraphicsContext _context;
        public BufferedGraphics Buffer;

        public static int Width;
        public static int Height;

        public BaseObject[] objects;
        public static Timer Timer;

        public Render(Form form)
        {
            InitGraphics(form);
            InitTimer();
        }

        void InitGraphics(Form form)
        {
            Graphics g;
            _context = BufferedGraphicsManager.Current;
            g = form.CreateGraphics();
            Width = form.Width;
            Height = form.Height;
            if (Buffer == null)
            {
                Buffer = _context.Allocate(g, new Rectangle(0, 0, Width, Height));
            }
        }

        void InitTimer()
        {
            Timer = new Timer { Interval = 1 };
            Timer.Start();
            Timer.Tick += TimerTick;
        }

        void TimerTick(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        public void Update()
        {
            foreach (BaseObject _object in objects)
            {
                _object.Update(Width);
            }
        }

        public void Draw()
        {

            Buffer.Graphics.Clear(Color.Black);

            foreach (BaseObject _object in objects)
            {
                if (_object.ObjectType == "Sprite")
                {
                    _object.DrawSprite(Buffer);
                }
                else
                {
                    _object.Draw(Buffer);
                }
            }

            Buffer.Render();
        }

    }
}
