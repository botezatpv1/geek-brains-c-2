﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace geek_brains_c_2
{
    class SplashScreen
    {
        public static int Width;
        public static int Height;

        public static Form form;

        static Random random = new Random();

        public static void Init(Form _form)
        {
            form = _form;

            Width = form.Width;
            Height = form.Height;

            LoadControls(form);
            Background.Init(form);
        }

        public static void LoadControls(Form form)
        {
            Button startGameButton = Utils.CreateButton(Color.White, "Start Game", "StartGame", new Point(350, 200),
                    new Size(100, 40), StartGameClick);
            Button scoreButton = Utils.CreateButton(Color.White, "Score", "Score", new Point(100, 500),
                   new Size(100, 40), StartGameClick);
            Button exitButton = Utils.CreateButton(Color.White, "Exit", "Exit", new Point(600, 500),
                   new Size(100, 40), ExitClick);
            Label nameLabel = Utils.CreateLabel("Pavel Botezat", new Point(300, 100), new Size(220, 40), 24);
            form.Controls.Add(startGameButton);
            form.Controls.Add(scoreButton);
            form.Controls.Add(exitButton);
            form.Controls.Add(nameLabel);
        }

        static void StartGameClick(Object sender, EventArgs e)
        {
            LevelManager.ChangeLevel(LevelManager.Levels.Game);
        }

        static void ExitClick(Object sender, EventArgs e)
        {
            Render.Timer.Stop();
            form.Close();
        }
    }
}
