﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace geek_brains_c_2
{
    static class Game
    {
        public static int Width;
        public static int Height;

        public static BaseObject[] objects;

        static Random random = new Random();

        public static void Init(Form form)
        {
            Width = form.Width;
            Height = form.Height;

            Load(form);
        }

        public static void Load(Form form)
        {
            objects = new BaseObject[550];
            for (int i = 0; i < objects.Length; i++)
            {
                int spawnPointX = random.Next(1, Width);
                int spawnPointY = random.Next(1, Height);
                int speedX = random.Next(1, 5);
                int speedY = random.Next(1, 5);
                int size = random.Next(3, 6);
                if (i < 450)
                {
                    objects[i] = new Dot(new Point(spawnPointX, spawnPointY), new Point(speedX, 0), new Size(1, 1));
                }
                else if (i < 540)
                {
                    objects[i] = new Star(new Point(spawnPointX, spawnPointY), new Point(speedX, 0), new Size(size, size));

                }
                else
                {
                    objects[i] = new BaseObject(new Point(spawnPointX, spawnPointY), new Point(speedX, speedY), new Size(size, size), "\\RockSprite.png",
                        new int[] { 5, 2 }, new int[] { random.Next(1, 6), random.Next(1, 3) });
                }
            }

            Render render = new Render(form);
            render.objects = objects;
        }

    }
}
