﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace geek_brains_c_2
{
    class Star : BaseObject
    {
        public Star(Point position, Point direction, Size size) : base(position, direction, size)
        {

        }

        public override void Draw(BufferedGraphics buffer)
        {
            buffer.Graphics.DrawLine(Pens.LightSlateGray, Position.X, Position.Y, Position.X + Size.Width, Position.Y + Size.Height);
            buffer.Graphics.DrawLine(Pens.LightSlateGray, Position.X + Size.Width, Position.Y, Position.X, Position.Y + Size.Height);
        }

        public override void Update(int GameWidth)
        {
            Position.X = Position.X - Direction.X;

            if (Position.X < 0)
            {
                Position.X = GameWidth + Size.Width;
            }

        }
    }
}
