﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace geek_brains_c_2
{
    class Dot : Star
    {
        public Dot(Point position, Point direction, Size size) : base(position, direction, size)
        {
        }

        public override void Draw(BufferedGraphics buffer)
        {
           buffer.Graphics.DrawEllipse(new Pen(Color.MidnightBlue), Position.X, Position.Y, Size.Width, Size.Height);
        }

    }
}
