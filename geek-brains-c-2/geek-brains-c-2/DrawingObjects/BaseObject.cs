﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace geek_brains_c_2
{
    class BaseObject
    {
        protected Point Position;
        protected Point Direction;
        protected Size Size;
        protected int[] Elements;
        protected int[] SelectedElement;

        protected Image Image;

        public string ObjectType { get; private set; }

        /// <summary>
        /// Creates Base Object (Circle);
        /// </summary>
        /// <param name="position">of object represented as two dimension point (x, y)</param>
        /// <param name="direction">of object represented as continiously changed point (x, y)</param>
        /// <param name="size">of object represented by two parameters (width, height)</param>
        /// <param name="image">string path to image from debug folder</param>
        /// <param name="elements">two dimensional array which represents number of elements in sprite sheet</param>
        /// <param name="selectedElement">tow dimensional array which represents element to be shown</param>
        public BaseObject(Point position, Point direction, Size size, string image = null, int[] elements = null, int[] selectedElement = null)
        {
            Position = position;
            Direction = direction;
            Size = size;
            ObjectType = "Figure";
            if (image != null)
            {
                Image = Image.FromFile(Application.StartupPath + $"\\Images\\{image}", true);
                Elements = elements;
                SelectedElement = selectedElement;
                ObjectType = "Sprite";
            }
        }
        
        /// <summary>
        /// Draw ellipise with given position and size
        /// </summary>
        public virtual void Draw(BufferedGraphics buffer)
        {
            buffer.Graphics.DrawEllipse(Pens.White, Position.X, Position.Y, Size.Width, Size.Height);
        }

        /// <summary>
        /// Draw a sprite from sprite sheet
        /// </summary>
        public virtual void DrawSprite(BufferedGraphics buffer)
        {
            if (Image != null)
            {
                int spriteWidth = Image.Width / Elements[0];
                int spriteHeight = Image.Height / Elements[1];

                Rectangle sprite = new Rectangle(Position.X, Position.Y, spriteWidth / Size.Width, spriteHeight / Size.Height);
                Rectangle spriteElement = new Rectangle(spriteWidth * SelectedElement[0] - spriteWidth, spriteHeight * SelectedElement[1] - spriteHeight, spriteWidth, spriteHeight);
                buffer.Graphics.DrawImage(Image, sprite, spriteElement, GraphicsUnit.Pixel);
            } else
            {
                throw new ArgumentException("You should specify Image path before trying to render sprite", "Image");
            }
        }

        /// <summary>
        /// Continuously moves point of ellipse and change it's direction
        /// if object hit border
        /// </summary>
        public virtual void Update(int GameWidth)
        {
            Position.X = Position.X + Direction.X;
            Position.Y = Position.Y + Direction.Y;

            if (Position.X <= 0 || Position.X >= GameWidth)
            {
                Direction.X = -Direction.X;
            }

            if (Position.Y <= 0 || Position.Y >= GameWidth)
            {
                Direction.Y = -Direction.Y;
            }
        }

    }
}
